# LIS4381 Mobile Web Application Development


## Mitchnikov Seide


### Assignment #1 Requirements:
1. Distributed version control with Git and Bitbucket
2. development installations
3. questions
4. bitcket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

### Git commands w/shrt descriptions
1. git init - creates new repository
2. git status - displays the current state
3. git add - adds all modified and new files
4. git commit - individual change to a file
5. git push - used to upload local repo
6. git pull - used to update the local version of a repo
7. git push origin master - push changes from all lcal branches to matching branches 

#### Assignment Screenshots:


*Screenshot code and output*:

![assignment 1 code and output](img/ampps.PNG)
*Screenshot of a1 code and output*:

![assignment 1 isntall proof](img/phone.PNG)

*Screenshot code and output*:

![assignment 1 code and output](img/java.PNG)
*Screenshot code and output*:

![assignment 1 code and output](img/hello.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ms17x/myteamquotes/ "My Team Quotes Tutorial")


