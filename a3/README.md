# LIS4381 Mobile Web Application Development

## Mitchnikov Seide


### Assignment #3 Requirements:

## Two Parts

- 1. create android application that can calculate price for tickets
- 2. create database for a pet store

### README.md file should incude the following item:

## screenshot of ERD
## screenshot of android app running first & second interface
## links to following files: a3.mb


#### Assignment Screenshots:


*Screenshot code ERD*:

![assignment 1 code and output](img/a3.PNG)

*Screenshot of android app first & second interface*:

![assignment 1 code and output](img/&&&&&&&.PNG) &&&&picture one

![assignment 1 code and output](img/&&&&&&&.PNG) &&&&picture two

[a3.mwb](docs/a3.mwb)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")

bitbucket.org:ms17x/lis4381.git


