import java.util.Scanner;

class LargestNumber
{
    public static void main(String args[])
    {
        //display operational messages
        System.out.println("program evaluates largest of two integers.");
        System.out.println("note: program does *not* check for non- numeric characters or non-integer values.");

        int num1;
        int num2;
        Scanner sc = new Scanner(System.in);

        System.out.print("enter first integer: ");
        num1 = sc.nextInt();

        System.out.print("enter second integer: ");
        num2 = sc.nextInt();

        System.out.println();
       if ( num1 > num2)
         System.out.println(num1 + " is larger than " + num2);
       else if ( num2 > num1)
         System.out.println(num2 + " is larger than " + num1);
       else
         System.out.println("integers are equal!.");
    }

}