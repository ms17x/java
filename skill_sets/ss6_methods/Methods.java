import java.util.Scanner;

class Methods
{
    public static void displayProgram()
    {
        System.out.println("2) myVoidMethod():\n" +
                           "\ta. accepts two arguments: String and int. \n" +
                           "\tb. prints use's first name and age.");
        System.out.println("3) myValueReturningMethod():\n" +
                           "ta. acepts two arguments: String and int. \n" +
                           "tb. returns string containing first name and age.");
        System.out.println();
    }

    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + "is" + age);
    }

    public static String myValueReturningMethod(String first, int age)
    {
        return first + "is" + age;
    }

    public static void main(String args[])
    {
        displayProgram();

        String firstName= "";
        int userAge = 0;
        String myStr= "";
        Scanner sc = new Scanner(System.in);

        System.out.print("enter first name: ");
        firstName=sc.next();

        System.out.print("enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        System.out.print("value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }
}