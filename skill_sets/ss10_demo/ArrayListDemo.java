import java.util.Scanner;
import java.util.*;

public class ArrayListDemo
{
    public static void main(String args[])
    {
    System.out.println("Program populates ArrayList of strings with user-entered animal type values.");
    System.out.println("examples: polar bear, guinea pig, dog, cat, bird.");
    System.out.println("program continues to collect user-entered values until user type\"n\".");
    System.out.println("program displays ArrayList values after each iteration, as well as size.");

    System.out.println();

    Scanner sc = new Scanner(System.in);
    ArrayList<String> obj = new ArrayList<String>();
    String myStr = "";
    String choice = "y";
    int num = 0;

    while (choice.equals("y"))
    {
        System.out.print("enter animal type: ");
        myStr = sc.nextLine();
        obj.add(myStr);
        num = obj.size();
        System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
        System.out.print("\nContinue? Enter y or n: ");
        choice = sc.next();
        sc.nextLine();
    }
    
    }
}