import java.util.Scanner;

public class NestedStructures
{
    public static void main(String args[])
    {
    System.out.println("Program searches user-entered integer w/in array of integers.");
    System.out.println("create an array with the following values: 3,2,4,99,-1,-5,3,7.");
    System.out.println();

    int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
    Scanner sc = new Scanner(System.in);
    int search;

    System.out.print("array length: " + nums.length);

    System.out.print("\nEnter search value: ");
    search = sc.nextInt();

    System.out.println();
    for (int i = 0; i < nums.length; i++)
    {
        if(nums[i] == search)
        {
            System.out.println(search + "found at index" + i);
        }
        else 
        {
            System.out.println(search + " *not* found at index " + i);
        }
    }


    
    
    }
}