import java.util.Scanner;

class DecisionStructures
{
    public static void main(String args[])
    {
        System.out.println("program evaluates user-entered characters.");
        System.out.println("use following characyers W or w, C or c, H of h, N or n.");
        System.out.println("use following decision structures: if...else, and switch.");
        System.out.println();

        String myStr="";
        char myChar=' '; 
        Scanner sc = new Scanner(System.in);

        System.out.println("phone types: W or w (work), C or c (cell), H of h (home), N or n (none).");
        System.out.print("enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println("\nif...else:");

        if(myChar == 'w')
           System.out.println("phone type: work");
        else if (myChar == 'c')
           System.out.println("phone type: cell");
        else if (myChar == 'h')
           System.out.println("phone type: home");
        else if (myChar == 'n')
           System.out.println("phone type: none");
        else
           System.out.println("incorrect character entry. ");

        System.out.println();
        System.out.println("switch:");
        switch (myChar)
        {
            case 'w':
             System.out.println("phone type: work");
             break;
            case 'c':
             System.out.println("phone type: cell");
             break;
            case 'h':
             System.out.println("phone type: home");
             break; 
            case 'n':
             System.out.println("phone type: none");
             break; 
            default:
             System.out.println("incorrect character entry. ");
             break;
        }               
    }
}