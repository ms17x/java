import java.util.Scanner;

class ArraysAndLoops
{
        public static void main(String args[]) 
    {
        System.out.println("program loops through array of strings");
        System.out.println("use following values: dog, cat, fish, bird, insect ");
        System.out.println("use following loop structures: for, enhanced for, while, do...while");
        System.out.println("\nNote: pretest loops: for, enhanced for, while, posttest loop: do...while");
        System.out.println();


        String animals[] = {"dog","cat","fish","bird","insect"};

        System.out.println("for loop: ");
        for(int i = 0; i < animals.length; i++)
        {
            System.out.println(animals[i]);
        }

        System.out.println("\nenhanced for loop");
        for(String test : animals)
        {
            System.out.println(test);
        }

        System.out.println("\nwhile loop");
        int i=0;
        while(i < animals.length)
        {
            System.out.println(animals[i]);
            i++;
        }

        System.out.println("\ndo while loop");
        i =0;
        do 
        {
            System.out.println(animals[i]);
            i++;
        }
        while(i < animals.length);

    }
     
}