import java.util.Scanner;

class LargestOfThreeNumbers
{
    public static void main(String args[])
    {
        System.out.println("program evaluates largest of three integers");
        System.out.println("note: program checks for integers and non- numeric values.");
        System.out.println();

        int num1 = 0, num2 = 0, num3 = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("please enter first number: ");
        while (!input.hasNextInt())
        {
          System.out.println("not valid integer!\n");
          input.next();
          System.out.print("please try again, enter first number: ");
        }
        num1 = input.nextInt();

        System.out.print("please enter second number: ");
        while (!input.hasNextInt())
        {
          System.out.println(" not valid");
          input.next();
          System.out.print("please try again, enetr second number: ");
        }
        num2 = input.nextInt();

        System.out.print("please enter third number: ");
        while (!input.hasNextInt())
        {
          System.out.println(" not valid integer!\n");
          input.next();
          System.out.print("please try again, enetr third number: ");
        }
        num3 = input.nextInt();

        System.out.println();

        if ( num1 > num2 && num1 > num3 )
          System.out.println("first number is largest. ");
        else if ( num2 > num1 && num2 > num3 ) 
           System.out.println("second number is largest. ");
        else if ( num3 > num1 && num3 > num2 ) 
           System.out.println("third number is largest. ");  
        else
           System.out.println("integers are equal.");   

    }    
 
}