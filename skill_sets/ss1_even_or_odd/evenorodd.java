import java.util.Scanner;
public class evenorodd
{
  public static void main (String[] args)
  {
      System.out.println("program evlauates integers as even or odd.");
      System.out.println("note: program does *not* check for non-numeric characters.");
      int x;
      System.out.println("enter a number: ");
      Scanner num = new Scanner(System.in);
      x = num.nextInt();
      if (x % 2 == 0)
      System.out.println(x + " is an even number. ");
      else 
      System.out.println(x + " is an odd number. ");
  }
}