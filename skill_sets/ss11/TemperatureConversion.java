import java.util.*;

class TemperatureConversion
{
public static void main(String[] args)
{
    System.out.println("Temperature Concersion Program\n");

    System.out.println("Program converts user-entered temperature into Fahrenheit of Celsisus scales ");
    System.out.println("Program continues to prompt for user entry untile no longer requested.");
    System.out.println("Note: upper or lower case letters permitted. Though, incorrect entreies are not permitted");
    System.out.println("Note: Program does not valifate numeric data (optional requirement)");

    Scanner sc = new Scanner(System.in);
    double temperature = 0.0;
    char choice = ' ';
    char type = ' ';

    do
    {
    System.out.print("\n Farhrenheit to Celsius? Type f, or Celsisus to Fahrenheit Type c: ");
    type = sc.next().charAt(0);
    type = Character.toLowerCase(type);
    if(type == 'f')
    {
    System.out.print("Enter temperature in Fahrenheit: ");
    temperature = sc.nextDouble();
    temperature = ((temperature - 32)*5)/9;
    System.out.println("Temperature in Celsius = " + temperature);
    }

    else if (type == 'c')
    {
    System.out.print("Enter temperature in Celsius: ");
    temperature = sc.nextDouble();
    temperature = (temperature *9/5) +32;
    System.out.println("Temperature in Fahrenheit = " + temperature);
    }
    else
    {
    System.out.println("Incorrect entry. Please try again. ");
    }

    System.out.print("\nDo you want t convert a temperature (y or n)? ");
    choice = sc.next().charAt(0);
    choice = Character.toLowerCase(choice);
    }

    while (choice == 'y');
    
    System.out.println("\nThank you for using our Temperature Conversion Program");
    }
}
